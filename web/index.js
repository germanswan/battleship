let myField = new Field;
const myFieldElement = document.getElementsByClassName('field')[0];
spawnFigure();
myField.update();
let fieldForServer = fieldToBoolean(myField.cells);
let enemyField = new Field;
enemyField.update();
document.body.addEventListener('getHit', (e) => {
  const hitField = myTurn ? enemyField : myField;
  let targetCell = hitField.cells[e.detail.coords.col][e.detail.coords.row];
  targetCell.dead = e.detail.hit;
  hitField.update();
  if (!e.detail.hit) {
    targetCell.marked = true;
  }
  if (e.detail.finished) {
    setDead(hitField.cells, [e.detail.coords.col, e.detail.coords.row]);
  }
  targetCell.update();
})
enemyField.fieldDiv.onclick = (e) => {
  if (e.target.className == 'cell') {
    let targetCell = enemyField.cells[e.target.coords.row][e.target.coords.col];
    if (!targetCell.dead || !targetCell.full) {
      send({
        action: 'makeTurn',
        coords: e.target.coords
      })
    }
  }
}
function setFulled(cells, coord) {
  for (let i = coord[0] - 1; i <= coord[0] + 1; i++) {
    for (let j = coord[1] - 1; j <= coord[1] + 1; j++) {
      if (getKey(myField.cells, i, j)) cells[i][j].full = true;
    }
  }
}
function setDead(cells, coord, checked = []) {
  for (let i = coord[0] - 1; i <= coord[0] + 1; i++) {
    for (let j = coord[1] - 1; j <= coord[1] + 1; j++) {
      if (getKey(cells, i, j)) {
        cells[i][j].marked = true;
        cells[i][j].update();
      }
      if (coord[0] == i && coord[1] == j) continue;
      if (getKey(cells, i, j, 'dead') && !checked.map(e => e.toString()).includes([i, j].toString())) {
        setDead(cells, [i, j], [...checked, [i, j]]);
      }
    }
  }
}
function spawnFigure() {
  let pool = [1, 1, 1, 1, 2, 2, 2, 3, 3, 4];
  let index;
  while (pool.length) {
    index = pool.length - 1;
    let coords = [];
    if (Math.round(Math.random())) { //положение (горизонтальное, вертикальное)
      coords = horisontal(coords);
    } else {
      coords = vertical(coords);
    }
    for (let i = 0; i < coords.length; i++) {
      if (getKey(myField.cells, getKey(coords, i, 0), getKey(coords, i, 1))) {
        let figureElem = myField.cells[coords[i][0]][coords[i][1]];
        figureElem.shaded = true;
        figureElem.full = true;
        setFulled(myField.cells, coords[i]);
      }
    }
    pool.splice(index, 1);
  }
  function searchCoords(factor, str, coords) {
    let col = Math.floor((Math.random() * 10));
    let row = Math.floor((Math.random() * 10));
    let newCol = col;
    let newRow = row;
    for (let i = 0; i < pool[index]; i++) {
      if (str == 'col') newCol = col + i * factor;
      if (str == 'row') newRow = row + i * factor;
      if (getKey(myField.cells, newCol, newRow, 'full') === false) {
        coords.push([newCol, newRow]);
        if (coords.length == pool[index]) {
          return true;
        }
      }
    }
  }
  function horisontal(coords) {
    while (true) {
      if (searchCoords(1, 'col', coords)) return coords;
      coords = [];
      if (searchCoords(-1, 'col', coords)) return coords;
      coords = [];
    }
  }
  function vertical(coords) {
    while (true) {
      if (searchCoords(1, 'row', coords)) return coords;
      coords = [];
      if (searchCoords(-1, 'row', coords)) return coords;
      coords = [];
    }
  }
}
function fieldToBoolean(field) {
  let result = [];
  for (let i = 0; i < 10; i++) {
    result.push(new Array());
    for (let j = 0; j < 10; j++) {
      result[i].push(field[j][i].shaded);
    }
  }
  return result;
}
