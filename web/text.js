let textDiv = document.createElement('div');
document.body.appendChild(textDiv);
let yourTurn = document.createElement('span');
textDiv.appendChild(yourTurn);
yourTurn.innerHTML = 'Your turn!';
let notYourTurn = document.createElement('span');
textDiv.appendChild(notYourTurn);
notYourTurn.innerHTML = 'It`s not your turn!';
yourTurn.hidden = true;
notYourTurn.hidden = true;

