class Field{
  constructor() {
    this.cells = [];
    this.fieldDiv = createDiv();
    document.body.appendChild(this.fieldDiv);
    this.fieldDiv.classList.add('field');
    for (let i = 0; i < 10; i++) {
      let colDiv = createDiv()
      this.fieldDiv.appendChild(colDiv);
      this.cells.push(new Array());
      for (let j = 0; j < 10; j++) {
        let cellDiv = createDiv()
        colDiv.appendChild(cellDiv);
        cellDiv.classList.add('cell');
        this.cells[i].push(new Cell(cellDiv, i, j));
      }
    }
  }
  update() {
    for (let i = 0; i < 10; i++) {
      for (let j = 0; j < 10; j++) {
        this.cells[i][j].update();
      }
    }
  }
}