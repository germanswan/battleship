function Cell(element, col, row) {
  this.shaded = false;
  this.marked = false;
  this.dead = false;
  this.full = false;
  this.element = element;
  this.element.coords = {row, col};
  this.update = function() {
    if (this.shaded) {
      setClass(this.element, 'shaded');
    }
    if (this.fulled || this.marked) {
      this.element.innerHTML = 'x';
    }
    if (this.dead) {
      setClass(this.element, 'dead');
    }
  }
}