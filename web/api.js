const HOST = location.origin.replace(/^http/, 'ws')
const ws = new WebSocket(HOST);
let myId;
let myTurn;
ws.onopen = () => { 
  setInterval(() => {
    send('ping');
  }, 30000);
};
ws.onmessage = (message) => {
  if (message.data == 'pong') return;
  let data = JSON.parse(message.data);
  if(data.type == 'id'){
    myId = data.id;
  }
  if(data.type == 'turn') {
    myTurn = myId == data.userId;
    yourTurn.hidden = !myTurn;
    notYourTurn.hidden = myTurn;
  }
  if(data.type == 'hit') {
    const event = new CustomEvent('getHit', {
      detail: data
    });
    document.body.dispatchEvent(event);
  }
  if(data.type == 'win'){
    if(myId == data.userId){
      myField.update();
      enemyField.update();
      setTimeout(() => {
        alert('Congratulations! You won!');
        location.reload()
      }, 500);
    }else {
      myField.update();
      enemyField.update();
      setTimeout(() => {
        alert('Better luck next time! You loose!');
        location.reload()
      }, 500);
    }
  }
}
ready.onclick = () => {
  send({
    action: 'setReady', 
    field: fieldForServer,
  })
  ready.hidden = true;
}

function send(data) {
  data = JSON.stringify(data);
  ws.send(data);
}
