const getKey = (object, ...keys) => {
  let result = object && objectCopy(object);
  for (const key of keys) {
    result = result && result[key];
    if (result === undefined) return;
  }
  return result;
}
function objectCopy(object){
  return JSON.parse(JSON.stringify(object));
}
function createDiv() {
  return document.createElement('div');
}
function setClass(element, className) {
  if (!element.classList.contains(className)) {
    element.classList.add(className);
  }
}