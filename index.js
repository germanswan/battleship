(async () => {
  require('./src/globals/logger');
  require('./src/globals/memoryDB');
  require('./src/globals/SocketError');
  const { run: test } = require('./test/index');
  const passed = await test();
  if (passed) require('./src/server');
})();
