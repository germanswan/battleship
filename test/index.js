const fs = require('fs');
const util = require('util');
const readdir = util.promisify(fs.readdir);

const tests = [];

class Test {
  constructor(name, f) {
    this.name = name;
    this.f = f;
    this.result = null;
  }
  run() {
    this.result = this.f();
  }
}

exports.test = function (name, f) {
  tests.push(new Test(name, f));
};

exports.run = async function () {
  const files = await readdir(__dirname);
  let failed = false;
  for (const file of files) {
    if(file === 'index.js') continue;
    require(`./${file}`);
    logger(1, ` ${file.split('.')[0].toLocaleUpperCase()}`);
    while(tests.length){
      const test = tests.shift();
      test.run();
      if (test.result) {
        logger(2, `${test.name}: ${test.result}`);
        failed = true;
      } else {
        logger(1, test.name);
      }
    }
  }
  return !failed;
}