const { test } = require('.');
const Field = require('../src/model/Field');

test('Create: empty array', () => {
  try {
    new Field([]);
  } catch (error) {
    return;
  }
  return 'Not throw error';
});

test('Create: arrays of falses', () => {
  try {
    new Field([
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
    ]);
  } catch (error) {
    return;
  }
  return 'Not throw error';
});

test('Create: 3 trues not in line', () => {
  try {
    new Field([
      [true, true, false, false, false, false, false, false, false, false],
      [true, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
    ]);
  } catch (error) {
    return;
  }
  return 'Not throw error';
});

test('Create: 5-deck', () => {
  try {
    new Field([
      [true, false, false, false, false, false, false, false, false, false],
      [true, false, false, false, false, false, false, false, false, false],
      [true, false, false, false, false, false, false, false, false, false],
      [true, false, false, false, false, false, false, false, false, false],
      [true, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
    ]);
  } catch (error) {
    return;
  }
  return 'Not throw error';
});

test('Create: valid data', () => {
  try {
    new Field([
      [true, false, true, true, true, false, false, false, false, false],
      [true, false, false, false, false, false, false, false, false, false],
      [true, false, true, true, false, false, false, true, false, false],
      [true, false, false, false, false, true, false, false, false, false],
      [false, false, true, false, false, false, false, false, false, false],
      [true, false, true, false, true, false, false, false, false, false],
      [true, false, false, false, false, false, false, false, true, false],
      [true, false, true, false, false, false, false, false, false, false],
      [false, false, true, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false, false],
    ]);
  } catch (error) {
    return error;
  }
});