function logger(level, ...args){
  const levels = {
    1: console.log,
    2: console.error,
  };
  levels[level](...args);
}

global.logger = logger;