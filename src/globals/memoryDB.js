global.users = [];

module.exports = {
  addUser(user) {
    users.push(user);
  },
  findUser(id) {
    return users.find(user => user.id === id);
  }
}