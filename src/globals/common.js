const objectCopy = (object) => JSON.parse(JSON.stringify(object));

const getKey = (object, ...keys) => {
  let result = object && objectCopy(object);
  for (const key of keys) {
    result = result && result[key];
    if (result === undefined) return;
  }
  return result;
}

const randomString = (length) => {
  let string = '';
  while (string.length < length) {
    string += Math.random().toString(36).substr(2, 10);
  }
  return string.substr(0, length);
};

const deleteFromArrayById = (array, f) => {
  const elementIndex = array.findIndex(f);
  array.splice(elementIndex, 1);
};

module.exports = {
  getKey,
  objectCopy,
  randomString,
  deleteFromArrayById,
};