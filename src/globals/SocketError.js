class SocketError {
  constructor(socket, data) {
    this.name = 'SocketError';
    this.data = data;
    this.socket = socket;
  }
}

global.SocketError = SocketError;