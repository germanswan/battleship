const express = require('express');
const SocketServer = require('ws').Server;
const router = require('./router/router');
const User = require('./model/User');
const { randomString, deleteFromArrayById } = require('./globals/common');
const { playersQueue, games } = require('./controllers/game');

const port = process.env.PORT || 8080;
const webFolder = __dirname.replace('src', 'web');
const server = express()
  .use((req, res) => res.sendFile(webFolder + req.path))
  .listen(port, () => console.log(`Listening on ${port}`));
const wss = new SocketServer({ server });

wss.on('connection', (socket) => {
  const user = new User(randomString(10), socket);
  users.push(user);
  logger(1, `user connected: ${user.id}`);
  socket.user_id = user.id;
  socket.sendJSON = function (data) {
    this.send(JSON.stringify(data));
  }
  let ping = setTimeout(()=> {
    socket.close();
  }, 50000);
  socket.on('message', async (message) => {
    clearTimeout(ping);
    ping = setTimeout(()=> {
      socket.close();
    }, 50000);
    await router(socket, message);
  });
  socket.on('close', async () => {
    clearTimeout(ping);
    deleteFromArrayById(users, user => user.id === socket.user_id);
    deleteFromArrayById(playersQueue, user_id => user_id === socket.user_id);
    games.end(socket.user_id, true);
    logger(1, `user disconnected: ${socket.user_id}`);
  });
});

logger(1, 'Socket server started');

module.exports = server;