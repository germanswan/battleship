module.exports = {
  parseMessage(message){
    return JSON.parse(message);
  },
  hasValidCoords({socket, data}) {
    if (!data.coords
      || typeof data.coords.row !== 'number'
      || typeof data.coords.col !== 'number'
      || data.coords.row < 0 || data.coords.row > 9
      || data.coords.col < 0 || data.coords.col > 9)
      throw new SocketError(socket, {coords: data.coords, message: 'Invalid coords'})
  }
}