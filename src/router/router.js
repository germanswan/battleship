const routes = require('./routes');
const handler = require('../errorHandler');
const middlewares = require('./middlewares');

async function router(socket, message) {
  const data = middlewares.parseMessage(message);
  if (data == 'ping') return socket.send('pong');
  const route = routes[data.action];
  if (!route) throw new SocketError(socket, 'RouteNotFound')
  for (const middleware of route.middleware) {
    await middleware({socket, data})
  }
  await route.handler({socket, data});
}

module.exports = handler(router);