const controllers = {};
controllers.game = require('../controllers/game');
middleware = require('./middlewares');

module.exports = {
  setReady: {
    middleware: [],
    handler: controllers.game.setReady,
  },
  makeTurn: {
    middleware: [middleware.hasValidCoords],
    handler: controllers.game.makeTurn,
  }
}