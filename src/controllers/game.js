const Game = require('../model/Game');
const Field = require('../model/Field');
const db = require('../globals/memoryDB');

const playersQueue = [];
const games = [];

function setReady({ socket, data }) {
  if (playersQueue.includes(socket.user_id))
    throw new SocketError(socket, 'You are already in queue');
  if (findGameWithUser(socket.user_id))
    throw new SocketError(socket, 'You are already in game');
  let field;
  try {
    field = new Field(data.field);
  } catch (error) {
    throw new SocketError(socket, { message: error.message, field: data.field });
  }
  const user = db.findUser(socket.user_id);
  user.setField(field);
  logger(1, 'user ready:', user.id);
  findPair(socket);
}

function findPair(socket) {
  const pairIndex = playersQueue.findIndex(id => id != socket.user_id);
  if (!~pairIndex) {
    playersQueue.push(socket.user_id);
    return logger(1, `${socket.user_id}: pair not found`);
  }
  const [pair_id] = playersQueue.splice(pairIndex, 1);
  logger(1, `${socket.user_id}: pair found: ${pair_id}`);
  const user = db.findUser(socket.user_id);
  const pair = db.findUser(pair_id);
  const game = new Game(pair, user);
  games.push(game);
}

function makeTurn({ socket, data }) {
  const game = findGameWithUser(socket.user_id);
  const player = db.findUser(socket.user_id);
  try {
    const end = game.makeTurn(player, data.coords);
    if (end) { 
      games.end(socket.user_id);
    }
  } catch (error) {
    throw new SocketError(socket, error.message);
  }
}

function findGameWithUser(id, index = false) {
  const method = index ? 'findIndex' : 'find';
  return games[method](game => game.players.map(player => player.id).includes(id));
}

games.end = function (userId, leave) {
  const gameIndex = findGameWithUser(userId, true);
  const [game] = games.splice(gameIndex, 1);
  if (!game) return;
  if (leave) {
    game.end(userId);
  }
}

module.exports = {
  playersQueue,
  setReady,
  makeTurn,
  games,
};