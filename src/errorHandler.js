function handle(error) {
  const socket = error.socket;
  delete error.socket;
  if (error.name !== 'SocketError') {
    if (socket) socket.sendJSON({
      error: {
        type: 'Server error',
        data: error,
      }
    })
    throw error;
  }
  delete error.name;
  logger(2, error);
  socket.sendJSON({error});
}

module.exports = function handler(func) {
  return async (...args) => {
    try {
      return await func(...args)
    } catch (error) {
      handle(error);
    }
  }
};