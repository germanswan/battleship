module.exports = class User {
  constructor(id, socket) {
    this.id = id;
    this.socket = socket;
    this.field = null;
  }
  setField(field) {
    this.field = field;
  }
}