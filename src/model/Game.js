module.exports = class Game {
  constructor(player1, player2) {
    this.players = [player1, player2];
    logger(1, 'new game:', player1.id, player2.id);
    this.turnCount = 0;
    this.allowedId = null;
    this.sendIds();
    this.setAllowed();
  }

  sendIds() {
    for (const player of this.players) {
      player.socket.sendJSON({
        type: 'id',
        id: player.id
      });
    }
  }

  setAllowed() {
    this.allowedId = this.players[this.turnCount % 2].id;
    this.sendToPlayers({
      type: 'turn',
      userId: this.allowedId,
    });
    logger(1, 'turn of user with id:', this.allowedId);
  }

  sendToPlayers(data) {
    for (const player of this.players) {
      player.socket.sendJSON(data);
    }
  }

  makeTurn(player, coords) {
    if (player.id !== this.allowedId) {
      logger(1, player.id, 'turn not allowed:', JSON.stringify(coords));
      throw new Error('It\'s not your turn');
    }
    logger(1, player.id, 'makes turn:', JSON.stringify(coords));
    const { field } = this.players.find(p => p.id != player.id);
    const {hit, finished} = field.hit(coords);
    logger(1, player.id, hit? 'hit' : 'not hit', JSON.stringify(coords));
    this.sendHit(coords, hit, finished);
    if (!hit) {
      this.nextTurn();
    }
    if (field.end()) {
      return this.end();
    }
  }

  sendHit(coords, hit, finished) {
    this.sendToPlayers({
      type: 'hit',
      coords,
      hit,
      finished,
    })
  }

  nextTurn(){
    this.turnCount++;
    this.setAllowed();
  }

  end(leaver) {
    if(leaver){
      const winner = this.players.find(p => p.id != leaver);
      winner.socket.sendJSON({
        type: 'win',
        userId: winner.id,
      })
    } else {
      this.sendToPlayers({
        type: 'win',
        userId: this.allowedId,
      });
    }
    return true;
  }
}