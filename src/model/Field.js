const { getKey } = require('../globals/common');

module.exports = class Field {
  constructor(field) {
    if (!this.isValid(field)) throw new Error('Invalid field');
    this.field = field.map(row => {
      return row.map(cell => ({
        ship: cell,
        hit: false
      }));
    });
  }

  isValid(field) {
    if (!Array.isArray(field) || field.length !== 10) return false;
    for (const row of field) {
      if (!Array.isArray(row) || row.length !== 10) return false;
      for (const bool of row) {
        if (typeof bool !== 'boolean') return false;
      }
    }
    const fieldObjects = [];
    for (let i = 0; i < field.length; i++) {
      fieldObjects[i] = [];
      for (let j = 0; j < field[i].length; j++) {
        fieldObjects[i][j] = {
          filled: field[i][j],
          checked: false,
        };
      }
    }

    const ships = {
      deck1: 0,
      deck2: 0,
      deck3: 0,
      deck4: 0,
    };

    for (let i = 0; i < fieldObjects.length; i++) {
      for (let j = 0; j < fieldObjects[i].length; j++) {
        if (!fieldObjects[i][j].filled) fieldObjects[i][j].checked = true;
        if (fieldObjects[i][j].checked) continue;
        if ((getKey(fieldObjects, i + 1, j + 1, 'filled'))
          || (getKey(fieldObjects, i + 1, j - 1, 'filled'))
          || (getKey(fieldObjects, i - 1, j + 1, 'filled'))
          || (getKey(fieldObjects, i - 1, j - 1, 'filled'))
        ) return false;
        const right = getKey(fieldObjects, i, j + 1, 'filled');
        const bottom = getKey(fieldObjects, i + 1, j, 'filled');
        if (right && bottom) return false;
        const direction = { i: 0, j: 0 };
        if (right) direction.j++;
        if (bottom) direction.i++;
        let decks = 1;
        fieldObjects[i][j].checked = true;
        while ((direction.i || direction.j)
          && getKey(fieldObjects, i + direction.i, j + direction.j, 'filled')) {
          decks++;
          fieldObjects[i + direction.i][j + direction.j].checked = true;
          direction.i += direction.i && 1;
          direction.j += direction.j && 1;
        }
        ships[`deck${decks}`]++;
      }
    }

    if (ships.deck1 !== 4
      || ships.deck2 !== 3
      || ships.deck3 !== 2
      || ships.deck4 !== 1
    ) return false;

    return true;
  }

  hit({ row, col }) {
    const cell = this.field[row][col];
    if (cell.hit) throw new Error('Cell is already hit');
    const hit = cell.ship;
    cell.hit = true;
    const finished = hit && this.checkFinished({ row, col });
    return { hit, finished };
  }

  checkFinished({ row, col }) {
    const ship = this.getShip({ row, col });
    for (const coord of ship) {
      const cell = this.field[coord.row][coord.col];
      if (!cell.hit) return false;
    }
    return true;
  }

  getShip({ row, col }, coords = []) {
    coords = [...coords, { row, col }];
    for (let i = row - 1; i <= row + 1; i++) {
      for (let j = col - 1; j <= col + 1; j++) {
        if ( i < 0 || i > 9
          || j < 0 || j > 9
          || coords.find(c => c.row == i && c.col == j)
        ) continue;
        const cell = this.field[i][j];
        if (cell.ship) {
          coords = this.getShip({ row: i, col: j }, coords);
        }
      }
    }
    return coords;
  }

  end() {
    for (const row of this.field) {
      for (const cell of row) {
        if (cell.ship && !cell.hit) return false;
      }
    }
    return true;
  }
}
